import socket
import ssl
import getpass
import MESSAGE_TYPE
import db_functions
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
import base64

clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientSocket.settimeout(10)
host = '127.0.0.1'
port = 6969
print('Waiting for connection response')

db = db_functions.wrapper_client()
db_functions.create_client_table(db)

# Generate an RSA asym key pair if not already exist
def gen_asym_keys():
    data = db_functions.get_data_srvdb(db, "Keys")
    if len(data[1]) == 0:
        #db.deleteDataInTable("Data" , 0 , commit = True , raiseError = True , updateId = True)
        key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        )

        privKeyByte = key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
            )

        pubKeyByte = key.public_key().public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo,
            )
        
        db_functions.insert_data_srvdb(db, "Keys", [privKeyByte, pubKeyByte])

gen_asym_keys()

try:
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    context.load_verify_locations('certs/serv.pem')
    context.check_hostname = False

    secureClientSocket  = context.wrap_socket(clientSocket)
    secureClientSocket.connect((host, port))
    peerCert = secureClientSocket.getpeercert()

except socket.error as e:
    print(str(e))

# res = secureClientSocket.recv(2048)
# print(res.decode('utf-8'))
registering = False
while True:
    try:
        res = secureClientSocket.recv(2048)
        type = res.decode('utf-8')[0]
        message = res.decode('utf-8')[1:]
        username = ""
        
        if type == MESSAGE_TYPE.HOME_TYPE:
            print(message)
            log_or_reg = input("[1] Login or [2] Register ?: > ")

            if log_or_reg.strip() == "1":
                print("Enter your Username")
                username = input('> ')
                secureClientSocket.send(str.encode(MESSAGE_TYPE.USERNAME_TYPE + username))
            elif log_or_reg.strip() == "2":
                print("Enter an Username")
                username = input('> ')
                registering = True
                secureClientSocket.send(str.encode(MESSAGE_TYPE.REGISTER_TYPE + username))
            else:
                print("Not a valid choice")
                quit()

        elif type == MESSAGE_TYPE.CMD_TYPE:
            print(message)
            type = None
            send_to_serv = False
            while not send_to_serv:            
                print("enter [Help] to show the available commandes")
                data = input('> ')

                if data == "Quit":
                    secureClientSocket.send(str.encode(MESSAGE_TYPE.CMD_TYPE + data))
                    secureClientSocket.close()
                    send_to_serv = True
                    quit()

                elif data == "Help":
                    print("You can perform the following action:")
                    print("[SendPrivate]: to send privates messages to someone of your contact list.")
                    print("[ShowContact]: to show your connected contact")
                    print("[AddContact]: to add someone to your contact list")
                    print("[Quit]: to disconnect")

                elif data == "SendPrivate":
                    send_to_serv = True
                    #secureClientSocket.send(str.encode(MESSAGE_TYPE.CMD_TYPE + data))
                    pass
                
                elif data == "ShowContact":
                    send_to_serv = True
                    #secureClientSocket.send(str.encode(MESSAGE_TYPE.CMD_TYPE + data))
                    pass

                elif data == "AddContact":
                    send_to_serv = True
                    #secureClientSocket.send(str.encode(MESSAGE_TYPE.CMD_TYPE + data))
                    pass

                else:
                    print("This is not a command")

        elif type == MESSAGE_TYPE.USERNAME_TYPE:
            type = None
            print(message)
            user = input('Username > ')
            secureClientSocket.send(str.encode(MESSAGE_TYPE.USERNAME_TYPE + user))   

        elif type == MESSAGE_TYPE.PASSWORD_TYPE:
            type = None
            print(message)
            verif = True

            while verif:
                if registering:
                    input1 = getpass.getpass(prompt="Password > ")
                    input2 = getpass.getpass("Enter again > ")

                    if input1 == input2:
                        secureClientSocket.send(str.encode(MESSAGE_TYPE.PASSWORD_TYPE + input1))
                        verif = False
                        registering = False
                    else:
                        print("Password did not match ! Try again: ")
            
                else:
                    input1 = getpass.getpass(prompt="Password > ")
                    verif = False
                    registering = False
                    secureClientSocket.send(str.encode(MESSAGE_TYPE.PASSWORD_TYPE + input1))
        
        elif type == MESSAGE_TYPE.PUBKEY_TYPE:
            print("User created succesfully")
            servPubKey = base64.b64decode(message)
            db = db_functions.wrapper_client()
            db_functions.insert_data_srvdb(db, "ServData", [servPubKey])
            secureClientSocket.send(str.encode(MESSAGE_TYPE.HOME_TYPE + ""))

    except KeyboardInterrupt:
        secureClientSocket.send(str.encode(MESSAGE_TYPE.CMD_TYPE + "quit"))
        quit()

clientSocket.close()