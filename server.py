import socket
from threading import Thread
import ssl
import hashlib
import os
import MESSAGE_TYPE
import db_functions
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
import base64

ServerSideSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = '127.0.0.1'
port = 6969


try:
    ServerSideSocket.bind((host, port))
except socket.error as e:
    print(str(e))

print('Socket is listening..')
ServerSideSocket.listen(5)

def gen_asym_keys():
    db = db_functions.wrapper_server()
    data = db_functions.get_data_srvdb(db, "Keys")
    if len(data[1]) == 0:
        #db.deleteDataInTable("Data" , 0 , commit = True , raiseError = True , updateId = True)
        key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        )

        privKeyByte = key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
            )

        pubKeyByte = key.public_key().public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo,
            )
        
        db_functions.insert_data_srvdb(db, "Keys", [privKeyByte, pubKeyByte])
        
        data = db_functions.get_data_srvdb(db, "Keys")

gen_asym_keys()


class multi_threaded_client(Thread):
    def __init__(self, connection, list_thread):
        Thread.__init__(self)
        self.context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        self.context.load_cert_chain('certs/serv.pem')
        self.secureClientSocket = self.context.wrap_socket(connection, server_side=True)
        
        self.list_thread = list_thread
        self.connected = True
        self.authenticated = False
        self.username = None

    def show_contact(self):
        for user in self.list_thread:
            if user.username != None:
                print(user.username)

    def hash_pass(self, salt, password):
        hash_pass = hashlib.pbkdf2_hmac('sha256',
                                    password.encode('utf-8'),
                                    salt,
                                    100000)
        return hash_pass 

    def verif_username(self, username):
        db = db_functions.wrapper_server()
        if db_functions.get_pass_db(db, username):
            return True
        else:
            return False

    def get_data_db(self):
        pass

    def sign_data(self):
        pass

    def sign_verif(self):
        pass

    def run(self) -> None:
        self.secureClientSocket.send(str.encode(MESSAGE_TYPE.HOME_TYPE + "Hello to super secure message app"))
        registering = False

        while self.connected:
            res = self.secureClientSocket.recv(2048)
            type = res.decode('utf-8')[0]
            message = res.decode('utf-8')[1:]

            if type == MESSAGE_TYPE.CMD_TYPE and self.authenticated:
                type = None
                if message == "Quit":
                    self.authenticated = False
                    self.connected = False
                    print(f"{self.username} disconnected")
                    self.secureClientSocket.close()
                else:
                    self.secureClientSocket.send(str.encode(MESSAGE_TYPE.CMD_TYPE + 'OK'))
                    self.show_contact()

            elif type == MESSAGE_TYPE.USERNAME_TYPE:
                type = None
                if self.verif_username(message):
                    self.secureClientSocket.send(str.encode(MESSAGE_TYPE.PASSWORD_TYPE + 'Enter your password: '))
                    self.username = message
                else:
                    self.secureClientSocket.send(str.encode(MESSAGE_TYPE.USERNAME_TYPE + 'Wrong username, enter again: '))
                    self.username = None
            
            elif type == MESSAGE_TYPE.PASSWORD_TYPE:
                type = None
                verif = True
                if registering:
                    salt = os.urandom(32)
                    hash_pass = self.hash_pass(salt, message)
                    db = db_functions.wrapper_server()
                    db_functions.insert_data_srvdb(db, "Users", [self.username, salt, hash_pass, []])
                    
                    data = db_functions.get_data_srvdb(db, "Keys")
                    pubKey64 = base64.b64encode(data[1][0][2]).decode()
                    self.secureClientSocket.send(str.encode(MESSAGE_TYPE.PUBKEY_TYPE + pubKey64))
                    message = None
                    registering = False
        
                else:
                    db = db_functions.wrapper_server()
                    salt_and_pass = db_functions.get_pass_db(db, self.username)
                    hash_pass = self.hash_pass(salt_and_pass[0], message)
                    
                    if salt_and_pass[1] == hash_pass:
                        self.secureClientSocket.send(str.encode(MESSAGE_TYPE.CMD_TYPE + 'Welcome !'))
                        print(f"{self.username} is connected")
                        self.authenticated = True
                    else:
                        self.secureClientSocket.send(str.encode(MESSAGE_TYPE.HOME_TYPE + 'Wrong password'))
            
            elif type == MESSAGE_TYPE.REGISTER_TYPE:
                type = None
                registering = True
                if not self.verif_username(message):
                    self.secureClientSocket.send(str.encode(MESSAGE_TYPE.PASSWORD_TYPE + 'Enter your password: '))
                    self.username = message
                else:
                    self.secureClientSocket.send(str.encode(MESSAGE_TYPE.HOME_TYPE + 'Username already exist try again: '))
                    self.username = None

            elif type == MESSAGE_TYPE.HOME_TYPE:
                self.secureClientSocket.send(str.encode(MESSAGE_TYPE.HOME_TYPE + "Hello to super secure message app"))
                registering = False

while True:
    all_thread = list()
    Client, address = ServerSideSocket.accept()
    print('Connected to: ' + address[0] + ':' + str(address[1]))
    t = multi_threaded_client(Client, all_thread)
    t.start()
    all_thread.append(t)
    

ServerSideSocket.close()