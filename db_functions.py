from pysqlitecipher import sqlitewrapper

def wrapper_server():
    obj = sqlitewrapper.SqliteCipher(dataBasePath="db_files/db_server.db" , 
                                    checkSameThread=False , 
                                    password="test")
    return obj

def wrapper_client():
    obj = sqlitewrapper.SqliteCipher(dataBasePath="db_files/db_client.db" , 
                                    checkSameThread=False , 
                                    password="test")
    return obj

def create_serv_table(db:sqlitewrapper.SqliteCipher):
    tables = {"Users": [
            ["username", "TEXT"],
            ["salt", "BLOB"],
            ["password", "BLOB"],
            ["contact", "LIST"]],

            "Keys":[
                ["privKey", "BLOB"],
                ["pubKey", "BLOB"],
            ]}

    for table in tables:
        db.createTable(table , tables[table] , makeSecure=True , commit=True)

def create_client_table(db:sqlitewrapper.SqliteCipher):
    tables = {"Keys": [
            ["privKey", "BLOB"],
            ["pubKey", "BLOB"]],

            "ServData": [
            ["pubKey", "BLOB"]],

            "Messages_histories": [
            ["Messages", "BLOB"]]
            }

    try:
        for table in tables:
            db.createTable(table , tables[table] , makeSecure=True , commit=True)
    except ValueError:
        pass

def insert_data_srvdb(db:sqlitewrapper.SqliteCipher, tableName: str, data: list):
    db.insertIntoTable(tableName , data , commit = True)

def get_data_srvdb(db:sqlitewrapper.SqliteCipher, tableName):
    datas = db.getDataFromTable(tableName , raiseConversionError = True , omitID = False)
    return datas

def get_pass_db(db:sqlitewrapper.SqliteCipher, username):
    datas = get_data_srvdb(db, "Users")[1]
    for data in datas:
       if data[1] == username:
           return data[2], data[3]
    return False

def get_contact_db(db:sqlitewrapper.SqliteCipher, username):
    datas = get_data_srvdb(db, "Users")[1]
    for data in datas:
       if data[1] == username:
           return data[4]
    return False

def update_user(db:sqlitewrapper.SqliteCipher, tableName, iDValue, colName, value):
    db.updateInTable(tableName , iDValue , colName , value , commit = True , raiseError = True)

if __name__ == '__main__':
    db = wrapper_server()
    create_serv_table(db)
    #insert_data_srvdb(db, "Users", ["HS", "aaa".encode(), "ttt".encode() ,["John", "Bob", "Alice"]])
    #print(get_pass_db(db, "Users"))